import Vue from 'vue'
import VueRouter from 'vue-router'
import HomeView from '../page/HomeView.vue'
import axios from 'axios'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'HomeView',
    component: HomeView
  },
  {
    path: '/login',
    name: 'Login',
  //   // route level code-splitting
  //   // this generates a separate chunk (about.[hash].js) for this route
  //   // which is lazy-loaded when the route is visited.
    component: () => import('../page/LoginPage.vue')
  },
  {
    path: '/register',
    name: 'Register',
    component: () => import('../page/RegisterPage.vue')
  },
  {
    path: '/admin',
    name: 'Admin',
    component: () => import('../page/AdminPage.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

const token = localStorage.getItem('token')
router.beforeEach(async (to, from ,next) => {
  if(to.name !== 'Login'){
    // if(!token) return next({name: 'Login'})
    const res = await axios.get("http://localhost:3001/me",{
      headers: { Authorization: "Bearer " + token },
    })
    if( res.status === 401 || !res.data ){
      return next({ name: 'Login' })
    }
  }
  return next()
})
export default router
