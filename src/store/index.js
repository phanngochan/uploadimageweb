import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    userlogged: ''
  },
  // mutations: {
  //   SET_USER_LOGGED(state, loggedUser) {
  //     state.userlogged = loggedUser;
  //   }
  // },
  // actions: {
  //   saveUserLogged(context, loggedUser) {
  //     context.commit("SET_USER_LOGGED", loggedUser);
  //   }
  // },
})
