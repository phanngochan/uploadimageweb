import axios from 'axios';

window.axios = axios;
axios.defaults.baseURL = 'http://localhost:3001/';
axios.interceptors.request.use(config=> {
    config.headers.common = {
        Authorization: `Bearer ${localStorage.getItem('token')}`,
        "Content-Type":"application/json",
        Accept: "application/json",
    }
    return config;
});